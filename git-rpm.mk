RPMDIRS = rpm-build rpm-dist
UPLOAD_FILES =

RPM_OPTS =
RPM_OPTS += --define "_sourcedir $${PWD}"
RPM_OPTS += --define "_builddir $${PWD}/rpm-build"
RPM_OPTS += --define "_srcrpmdir $${PWD}/rpm-dist"
RPM_OPTS += --define "_rpmdir $${PWD}/rpm-dist"
RPM_OPTS += --define "_specdir $${PWD}"

NAME ?= packagename-to-be-defined

SPEC = $(NAME).spec

RPM_SUMMARY  = $(shell rpm $(RPM_OPTS) -q --specfile $(SPEC) --queryformat '[%{SUMMARY}\n]' | sed '1q')
RPM_URL      = $(shell rpm $(RPM_OPTS) -q --specfile $(SPEC) --queryformat '[%{URL}\n]' | sed '1q')

RPM_VERSION := $(shell rpm $(RPM_OPTS) -q --specfile $(SPEC) --queryformat '[%{VERSION}\n]' | sed '1q')
RPM_RELEASE := $(shell rpm $(RPM_OPTS) -q --specfile $(SPEC) --queryformat '[%{RELEASE}\n]' | sed '1q')
RPM_ARCH    := $(shell rpm $(RPM_OPTS) -q --specfile $(SPEC) --queryformat '[%{ARCH}\n]' | sed '1q')

NVR = $(NAME)-$(RPM_VERSION)-$(RPM_RELEASE)

SRPM = rpm-dist/$(NVR).src.rpm
BRPM = rpm-dist/$(RPM_ARCH)/$(NVR).$(RPM_ARCH).rpm

-include $(HOME)/.git-rpm-settings.mk
RSYNC_USER ?= $(USER)
RSYNC_HOST ?= fedorapeople.org
RSYNC_USHO ?= $(RSYNC_USER)@$(RSYNC_HOST)
RSYNC_ROOT ?= public_html/packages
RSYNC_DIR = $(RSYNC_ROOT)/$(NAME)/$(RPM_VERSION)-$(RPM_RELEASE)

SOURCE_FILES = $(shell spectool --lf $(SPEC) | cut -d' ' -f2 | xargs -l basename)

RPMBUILD_OPTS =
RPMBUILD_OPTS += --buildroot="$${PWD}/rpm-build/$(NVR)-buildroot"

.PHONY: all
all: build
build: rpm check

.PHONY: sources
sources:
	spectool -g $(SPEC)

.PHONY: srpm
srpm: $(SRPM)

clog: $(SPEC)
	rpm $(RPM_OPTS) -q --specfile $(SPEC) --changelog | sed '1n; /^\*/,$$d' > $@
	cat $@

UPLOAD_FILES += $(NVR)/$(NVR).src.rpm
$(SRPM): $(SPEC) $(SOURCE_FILES)
	mkdir -p $(RPMDIRS)
	rpmbuild $(RPM_OPTS) $(RPMBUILD_OPTS) -bs $(SPEC)
$(NVR)/$(NVR).src.rpm: $(SRPM)
	mkdir -p $(NVR)
	cp -p $< $@

.PHONY: rpm
rpm: $(BRPM)

UPLOAD_FILES += $(NVR)/build.log
$(BRPM) rpm-build/$(NVR).buildlog: $(SPEC) $(SOURCE_FILES)
	mkdir -p $(RPMDIRS)
	rpmbuild $(RPM_OPTS) $(RPMBUILD_OPTS) -ba $(SPEC) 2>&1 | tee rpm-build/$(NVR).buildlog

$(NVR)/build.log: rpm-build/$(NVR).buildlog
	mkdir -p $(NVR)
	sed "s|$(USER)|build|g" < $< > $@

.PHONY: check
check: $(NVR)/rpmlint.txt $(NVR)/filelist.txt

UPLOAD_FILES += $(NVR)/rpmlint.txt
$(NVR)/rpmlint.txt: $(BRPM)
	mkdir -p $(NVR)
	rpmlint -i $(SPEC) $(SRPM) $(BRPM) 2>&1 | tee $@

UPLOAD_FILES += $(NVR)/filelist.txt
$(NVR)/filelist.txt: rpm-build/$(NVR).buildlog
	mkdir -p $(NVR)
	for pkg in rpm-dist/$(NVR).src.rpm rpm-dist/*/*-$(RPM_VERSION)-$(RPM_RELEASE).*.rpm; do \
		echo "## $$(basename "$$pkg")"; \
		rpm -qplv --scripts "$$pkg"; \
		echo; \
	done > $@.new
	cp $@.new $@ && rm -f $@.new

REPODIR = $(NAME)-package.git
.PHONY: setup
setup:
	ssh $(RSYNC_USHO) mkdir -p $(RSYNC_ROOT)/$(NAME) public_git
	ssh $(RSYNC_USHO) "test -d public_git/$(REPODIR) || env GIT_DIR=public_git/$(REPODIR) git --bare init"
	ssh $(RSYNC_USHO) "echo 'git://fedorapeople.org/~$(RSYNC_USER)/$(REPODIR)' > public_git/$(REPODIR)/cloneurl"
	ssh $(RSYNC_USHO) "touch public_git/$(REPODIR)/git-daemon-export-ok"
	ssh $(RSYNC_USHO) "echo '$(NAME) RPM package ($(RPM_SUMMARY))' > public_git/$(REPODIR)/description"
	ssh $(RSYNC_USHO) "echo '<p>RPM package for <a href=\"$(RPM_URL)\">$(NAME)</a>: $(RPM_SUMMARY)</p><p>See also the finished <a href=\"http://$(RSYNC_USER).$(RSYNC_HOST)/packages/$(NAME)/\">$(NAME) rpm packages</a>.</p>' > public_git/$(REPODIR)/README.html"
	ssh $(RSYNC_USHO) "echo '<p>RPM package for <a href=\"$(RPM_URL)\">$(NAME)</a>: $(RPM_SUMMARY)</p><p>See also the <a href=\"http://$(RSYNC_HOST)/gitweb?p=$(RSYNC_USER)/public_git/$(REPODIR);a=summary\">$(NAME) rpm package git repository</a>.</p>' > $(RSYNC_ROOT)/$(NAME)/99README.html"

STUFF_TO_PUSH  =
STUFF_TO_PUSH += refs/heads/master:refs/heads/master
# STUFF_TO_PUSH += refs/tags/$(NVR):refs/tags/$(NVR)
STUFF_TO_PUSH += $(foreach x,$(shell git tag -l),refs/tags/$(x):refs/tags/$(x))
.PHONY: push
push:
	git push "ssh://$(RSYNC_USER)@$(RSYNC_HOST)/~/public_git/$(REPODIR)" $(STUFF_TO_PUSH)

.PHONY: upload
upload: $(NVR) tag push

UPLOAD_FILES += $(NVR)/$(SPEC)
$(NVR)/$(SPEC): $(SPEC)
	mkdir -p $(NVR)
	cp -p $< $@

UPLOAD_FILES += $(foreach p, $(SOURCE_FILES), $(NVR)/$(p))
$(NVR)/%: %
	mkdir -p $(NVR)
	cp -p $< $@

$(NVR): $(UPLOAD_FILES)
	mkdir -p $(NVR)
	rsync -avz --delete $(foreach x,$(shell git tag -l),--link-dest=../$(subst $(NAME)-,,$(x))) $(NVR)/ $(RSYNC_USHO):$(RSYNC_DIR)/
	touch $@

.PHONY: clean
clean:
	rm -rf $(RPMDIRS)

.PHONY: tag
tag: clog
	@echo "List of git tags:"; git tag -n20 -l '$(NAME)*'
	if tag_sha=`git rev-list tags/$(NVR) | sed 1q` && test -n "$$tag_sha"; then \
		head_sha=`git rev-list HEAD | sed 1q`; \
		if test "x$$tag_sha" = "x$$head_sha"; then \
			echo "Tag $(NVR) already exists and equal to HEAD."; \
		else \
			echo "Error: Tag $(NVR) already exists but not equal to HEAD."; \
			exit 13; \
		fi; \
	else \
		echo "Really add git tag $(NVR)? Ctrl-C to abort, Enter to continue"; \
		read; \
		echo git tag -a -F clog $(NVR); \
		git tag -a -F clog $(NVR); \
	fi

WORKDIR ?= $(PWD)
MOCKDIR ?= $(WORKDIR)
MOCK_ARCH ?= $(RPM_ARCH)
MOCK_FEDORA_RELEASE ?= $(shell echo $(RPM_RELEASE) | sed -n 's/.*\.fc\([1-9][0-9]*\)$$/\1/p')
MOCKCFG ?= fedora-$(MOCK_FEDORA_RELEASE)-$(MOCK_ARCH)
MOCKRESULTDIR ?= $(MOCKDIR)/$(NVR).mock

SCRATCHBUILD_KOJI_TAG = dist-f13

.PHONY: scratch-build
scratch-build: $(SRPM)
	/usr/bin/koji build --scratch $(SCRATCHBUILD_KOJI_TAG) $(SRPM)

.PHONY: mock-build
mock-build: $(MOCKRESULTDIR)

$(MOCKRESULTDIR): $(SRPM)
	mock $(MOCKARGS) -r $(MOCKCFG) --resultdir=$(MOCKRESULTDIR) rebuild $(SRPM)

.PHONY: help
help:
	@echo "NVR:             $(NVR)"
	@echo "Summary:         $(RPM_SUMMARY)"
	@echo "Mock config:     $(MOCKCFG)"
	@echo "Mock result dir: $(MOCKRESULTDIR)"
	@echo "Source RPM:      $(SRPM)"
	@echo "Targets:"
	@echo "  build          build the RPM package(s) locally"
	@echo "  scratch-build  build scratch RPM via Fedora's koji"
	@echo "  mock-build     build RPM in local mock chroot"
	@echo "  sources        download all source files"
	@echo "  srpm           build SRPM package locally"

# Package specific rules, best put into package specific GNUmakefile

#$(NAME)-$(RPM_VERSION)-buildfixes.patch: $(wildcard $(NAME)-$(RPM_VERSION)/* $(NAME)-$(RPM_VERSION)-buildfixes/*)
#	diff -ru $(NAME)-$(RPM_VERSION) $(NAME)-$(RPM_VERSION)-buildfixes > $@;:

# End of file.
